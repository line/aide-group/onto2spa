# Ontology mapping onto a Semantic Pointer Architecture

## Abstract

Some human cognitive tasks may involve tightly interleaved logical and numerical computations, hence calling for both symbolic and sub-symbolic approaches. On the one hand, ontologies allow us to describe symbolic structured knowledge and perform logical inference, providing a rather natural representation of human reasoning as modeled in cognitive psychology. On the other hand, spiking neural networks are a biologically plausible implementation of learning in brain circuits, yet they process numeric vectors rather than symbolic data. Unifying these symbolic and sub-symbolic approaches is still a wide and open question in Artificial Intelligence. In that direction, the Semantic Pointer Architecture (SPA) based on the Vector Symbolic Architecture (VSA) provides a way to manipulate symbols embedded as numeric vectors that carry semantic information.

As a step towards filling the symbolic/numerical gap, we propose to map an ontology onto a SPA-based architecture with a preliminary partial implementation into spiking neural networks. More specifically, we focus on ontology standards used in the semantic web such as Resource Description Framework [Schema] (RDF[S]) and the Web Ontology Language (OWL). We provide a detailed implementation example in the case of specific RDFS entailments based on predicate chaining. To that end, we used the neural simulator Nengo with two associative memories in interaction, the first one storing assertions and the second one storing entailment rules. Reporting interesting formal results, our embedding enjoys intrinsic properties allowing semantic reasoning through distributed numerical computing. This original preliminary work thus combines symbolic and numerical approaches for cognitive modeling, which might be useful to model some complex human tasks such as ill-defined problem-solving, involving neuronal knowledge manipulation.

## Partial implementation

In the notebook main.ipynb, we propose an example of architecture capable of class subsumption entailment, using Nengo modules (see the documentation for NengoSPA [here](https://www.nengo.ai/nengo-spa/)) and we test this architecture on a small subset of ontology inspired by the [Pizza ontology](https://github.com/owlcs/pizza-ontology).

## Install and run

We recommand using [Pipenv](https://pipenv.pypa.io/en/latest/) to manage Python environments.

After installing Pipenv, install the required dependencies with:

```
pipenv install
```

Then activate the environment and launch Jupyter:

```
pipenv shell
jupyter notebook
```
